from typing import Union
import joblib
from fastapi import FastAPI
from pydantic import BaseModel
import numpy as np
import pandas as pd
from fastapi.middleware.cors import CORSMiddleware
from fastapi import FastAPI, Form
from typing import Annotated


app = FastAPI()
origins = [
    "http://localhost:5173"
]
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

def add_feature(X, feature_to_add):

    from scipy.sparse import csr_matrix, hstack
    return hstack([X, csr_matrix(feature_to_add).T], 'csr')

model = joblib.load('email.pkl')
vect = joblib.load('vect.pkl')
answer = ['Not Spam', 'Spam']
class Email(BaseModel):
    Text: str

@app.get("/")
def read_root():
    return {"Hello": "World"}


@app.put("/Email")
async def update_item(item: Annotated[str, Form()]):
    data = np.array(
    [
        [item]
    ])
    data = pd.DataFrame(data)
    data = data.loc[0]
    add_length_t=data.str.len()
    add_digits_t=data.str.count(r'\d')
    add_dollars_t=data.str.count(r'\$')
    add_characters_t=data.str.count(r'\W')
    data_transformed = add_feature(vect.transform(data), [add_length_t, add_digits_t,  add_dollars_t, add_characters_t])
    prediction = model.predict(data_transformed)
    print(answer[prediction[0]])
        
    return {"prediction" : answer[prediction[0]]}